# RespectX Standard Library #

This project aim is to provide a standard library for the [RespectX](https://bitbucket.org/gciatto/respectx/) language.
Predicates and reactions in such a library are meant to ease the development of interaction/coordination protocols/rules, by incapsulating well studied patterns within RespectX modules.

## Currently supports ##

* The `rsp.timing.Periodic` module, providing a way to schedule a periodic activities,
    - which in turns eases the implementation of time-dependent mechanisms;

* The `rsp.lang.Concentration` module, providing reaction enabling Linda's primitives to work with (concentration-)enriched tuples;

* The `rsp.land.Decay` module, implementing the time-dependent mechanism making some enriched tuple concentration decay as time flows;

* The `rsp.land.Neighborhood` module, sopporting the inter-connection of tuple centers and, therefore, the concept of "tuple centers network",
    - which in turns enables the implementation of decentralized and network-assuming algorithms;

* The `rsp.land.Spreading` module, providing a mean to spread tuples over a network of tuple centers. 

## Environment setup ##
* This project requires a running RespectX-enabled Eclipse instance
    - Please refer to [RespectX](https://bitbucket.org/gciatto/respectx/) to get one
* If the `src-gen` folder is empty, you probably should regenerate specifications by hand. To do so:
    * Open each specification source file
    * Apply some trivial edit to the specification file, e.g. by adding some withespace
    * Save the file
    * A ReSpecT (`.rsp`) specification file will be generated within the `src-gen` source folder for each RespectX specficiation file contained into the other source folders
    * You can load such generated `.rsp` files on a [TuCSoN](http://apice.unibo.it/xwiki/bin/view/TuCSoN/WebHome) tuple center
* Enjoy


